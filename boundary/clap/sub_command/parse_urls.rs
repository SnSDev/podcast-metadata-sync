use super::*;
use crate::presenter::{
    compile_problem::prelude::*, relative_file_path::prelude::*,
    url::prelude::*,
};
use markdown_database_lib::gateway::file_system::{
    PhysicalFS, VfsError, VfsPath,
};
use markdown_database_lib::parse_markdown_file_use_case::*;
use markdown_database_lib::wrapper::VfsPathParseRelativePath;
use std::io::Error;
use std::path::Path;
use stdio_iterators::pipe_out;

const ARG_FILE_PATH: &str = "FILE_PATH";

pub fn build_sub_command<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name(COMMAND_NAME)
        .about(SHORT_DESCRIPTION)
        .author(crate_authors!())
        .version(crate_version!())
        .long_about(LONG_DESCRIPTION)
        .arg(
            Arg::with_name(ARG_FILE_PATH)
                .help("Path to markdown file")
                .required(true)
                .index(1),
        )
}

pub const PARSABLE_MATCH: ParsableMatch = ParsableMatch {
    check_for_match_closure: |matches| -> bool {
        matches.is_present(COMMAND_NAME)
    },
    use_case_to_run: |matches| -> MainReturn {
        let root = VfsPath::new(PhysicalFS::new(Path::new("/").into()));
        let current_dir = std::env::current_dir()
            .expect("1632029064 - Current Directory Not Found");

        let base_dir = match current_dir.to_string_lossy().as_ref() {
            "/" => root,
            current_dir => root
                .join(current_dir.strip_prefix('/').expect(
                    "1632525771 - Unreachable: Previous match handles root \
                     case",
                ))
                .expect(
                    "1632453633 - Unreachable: std::env::current_dir is \
                     always a valid path",
                ),
        };

        let matches = matches
            .subcommand_matches(COMMAND_NAME)
            .expect("1630298058 - Unreachable! Required Parameters Missing");

        let file_path = match base_dir.parse_relative_path(
            matches
                .value_of(ARG_FILE_PATH)
                .expect("1630298284 - Unreachable! Required Parameter Missing"),
        ) {
            Ok(p) => p,
            Err(VfsError::InvalidPath { path }) => {
                // TODO: VfsPath Error thrown funny
                // ```
                // Error: Custom { kind: Other, error: "WithContext { context: \"Could not get metadata for '/home/Dropbox/Pim/index.md'\", cause: IoError(Os { code: 2, kind: NotFound, message: \"No such file or directory\" }) }" }
                //
                return Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidInput,
                    format!("1632526180 - Invalid path {:?}", path),
                ));
            }
            Err(other) => {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("1632526167 - {:?}", other),
                ))
            }
        };
        let markdown_file_contents =
            file_path.read_to_string().map_err(|err| {
                // TODO: Verify correct error handler
                Error::new(std::io::ErrorKind::Other, format!("{:?}", err))
            })?;
        let request_model = ParseMarkdownFileRequestModel {
            file_contents: markdown_file_contents,
            file_path,
        };

        //--------------------------------------------------------------

        let response_model: ParseMarkdownFileResponseModel =
            parse_markdown_file(&request_model).map_err(|error| {
                eprintln!("{:#?}", error);
                std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "1628034091 - Failed to parse-urls in file",
                )
            })?;

        //--------------------------------------------------------------

        let relative_path_presenter = RelativeFilePathPresenter {
            base_dir: &base_dir,
        };
        let problem_presenter = CompileProblemPresenter {
            path_presenter: &relative_path_presenter,
        };
        let url_presenter = UrlPresenter::default();
        let arena_url = typed_arena::Arena::new();
        let arena_path = typed_arena::Arena::new();

        //--------------------------------------------------------------

        use markdown_database_lib::data_object::MarkdownLink::*;
        let pipe_results =
            pipe_out(response_model.results.filter_map(|result| {
                let display: Option<Box<dyn std::fmt::Display>> = match result {
                    Ok(Url(url)) => Some(Box::new(
                        url_presenter.render(arena_url.alloc(url)),
                    )),
                    Ok(LocalFile(local_file_path)) => Some(Box::new(
                        relative_path_presenter
                            .render(arena_path.alloc(local_file_path)),
                    )),
                    Ok(None) => Option::None,
                    Err(compile_problem) => {
                        eprintln!(
                            "{}",
                            problem_presenter.render(&compile_problem)
                        );
                        Option::None
                    }
                };

                display
            }));

        //--------------------------------------------------------------

        match pipe_results {
            Ok(true) => Ok(()),
            Ok(false) => Err(std::io::Error::new(
                std::io::ErrorKind::BrokenPipe,
                "1628034295 - Broken Pipe",
            )),
            Err(err) => {
                Err(std::io::Error::new(std::io::ErrorKind::Other, err))
            }
        }
    },
};
