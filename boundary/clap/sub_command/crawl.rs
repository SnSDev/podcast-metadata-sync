use super::*;
use crate::presenter::{
    colorizer::{Theme, ThemeMatchVscodeDarkPlus},
    compile_problem::CompileProblemPresenter,
    prelude::Presenter,
    relative_file_path::RelativeFilePathPresenter,
    url::UrlPresenter,
};
use markdown_database_lib::{
    crawl_database_files_use_case::*, entity::SpiderMessage,
};
use std::{
    io::{Error, ErrorKind},
    sync::mpsc,
};
use vfs::{PhysicalFS, VfsPath};

type Colorizer = ThemeMatchVscodeDarkPlus;

pub fn build_sub_command<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name(COMMAND_NAME)
        .about(SHORT_DESCRIPTION)
        .author(crate_authors!())
        .version(crate_version!())
        .long_about(LONG_DESCRIPTION)
}

pub const PARSABLE_MATCH: ParsableMatch = ParsableMatch {
    check_for_match_closure: |matches| -> bool {
        matches.is_present(COMMAND_NAME)
    },
    use_case_to_run: |_| -> MainReturn {
        let base_dir: VfsPath =
            PhysicalFS::new(std::env::current_dir().unwrap()).into();

        let (tx, rx) = mpsc::channel();

        let request_model = CrawlDatabaseFilesRequestModel {
            real_time_output: tx,
            base_dir: base_dir.clone(), // TODO: Replace with borrow *
                                        /*
                                        Unable to borrow

                                        `base_dir` does not live long enough
                                        borrowed value does not live long enoughrustcE0597
                                        crawl.rs(83, 5): `base_dir` dropped here while still borrowed
                                        crawl.rs(51, 22): argument requires that `base_dir` is borrowed for `'static`

                                        Borrow should end at same time CrawlDatabaseFilesRequestModel is dropped
                                        */
        };

        let local_file_presenter = RelativeFilePathPresenter {
            base_dir: &base_dir,
        };

        let compile_problem_presenter = CompileProblemPresenter {
            path_presenter: &local_file_presenter,
        };

        let url_presenter = UrlPresenter {};

        let result = std::thread::spawn(|| crawl_database_files(request_model));

        for sig in rx.into_iter() {
            match sig {
                SpiderMessage::CompileProblem(problem) => {
                    eprintln!("{}", compile_problem_presenter.render(&problem))
                }
                SpiderMessage::IoError(text) => {
                    eprintln!(
                        "{} {:?}",
                        Colorizer::Error.apply("Unexpected I/O Error"),
                        text
                    )
                }
                SpiderMessage::FileNotFound(file) => {
                    println!(
                        "{}: {}",
                        Colorizer::FileNotFound.apply("Not Found "),
                        Colorizer::StringLiteral.apply(&format!(
                            "{}",
                            local_file_presenter.render(&file)
                        )),
                    )
                }
                SpiderMessage::Success(file) => {
                    println!(
                        "{}: {}",
                        Colorizer::Success.apply("Parsed    "),
                        Colorizer::StringLiteral.apply(&format!(
                            "{}",
                            local_file_presenter.render(&file)
                        )),
                    )
                }
                SpiderMessage::Url(url) => {
                    println!(
                        "{}: {}",
                        Colorizer::Info.apply("URL Skip  "),
                        Colorizer::FunctionDeclaration
                            .apply(&format!("{}", url_presenter.render(&url),)),
                    )
                }
                SpiderMessage::FileExists(path) => {
                    println!(
                        "{}: {}",
                        Colorizer::Success.apply("Exists    "),
                        local_file_presenter.render(&path),
                    )
                }
            }
        }

        result.join().map(|_| ()).map_err(|_| {
            Error::new(ErrorKind::Other, "1633478981 - Unknown error")
        })
    },
};
