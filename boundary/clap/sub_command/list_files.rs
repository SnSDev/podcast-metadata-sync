use super::*;
use crate::presenter::relative_file_path::prelude::*;
use markdown_database_lib::gateway::file_system::{PhysicalFS, VfsPath};
use markdown_database_lib::list_markdown_files_use_case::*;

pub fn build_sub_command<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name(COMMAND_NAME)
        .about(SHORT_DESCRIPTION)
        .author(crate_authors!())
        .version(crate_version!())
        .long_about(LONG_DESCRIPTION)
}

pub const PARSABLE_MATCH: ParsableMatch = ParsableMatch {
    check_for_match_closure: |matches| -> bool {
        matches.is_present(COMMAND_NAME)
    },
    use_case_to_run: |_| -> MainReturn {
        let base_dir: VfsPath =
            PhysicalFS::new(std::env::current_dir().unwrap()).into();

        let request_model = ListMarkdownFilesRequestModel {
            base_dir: base_dir.clone(),
        };

        let presenter = RelativeFilePathPresenter {
            base_dir: &base_dir,
        };

        match list_markdown_files(request_model) {
            Ok(response_model) => {
                let arena = typed_arena::Arena::new();
                match stdio_iterators::pipe_out(response_model.iterator.map(
                    |unix_absolute_path| {
                        presenter.render(arena.alloc(unix_absolute_path))
                    },
                )) {
                    Ok(true) => Ok(()),
                    Ok(false) => Err(std::io::Error::new(
                        std::io::ErrorKind::BrokenPipe,
                        "1627520497 - Broken Pipe",
                    )),
                    Err(e) => Err(e),
                }
            }
            Err(_) => unreachable!("1627517244"),
        }
    },
};
