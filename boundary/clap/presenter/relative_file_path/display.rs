use super::*;
pub struct RelativeFilePathDisplay<'a> {
    pub settings: &'a RelativeFilePathPresenter<'a>,
    pub data: &'a VfsPath,
}

impl<'a> Display for RelativeFilePathDisplay<'a> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            fmt,
            "{}",
            self.data
                .relative_to(self.settings.base_dir)
                .replace(" ", "\\ ")
        )
    }
}

#[cfg(test)]
mod test {
    use pretty_assertions::assert_eq;

    #[test]
    fn test1633560969_path_with_spaces() {
        use crate::presenter::prelude::Presenter;
        use crate::presenter::relative_file_path::RelativeFilePathPresenter;
        use vfs::{MemoryFS, VfsPath};

        let root = VfsPath::new(MemoryFS::default());
        let test_path = root.join("path with spaces.file").unwrap();
        let presenter = RelativeFilePathPresenter { base_dir: &root };

        let actual = presenter.render(&test_path);

        assert_eq!(r#"./path\ with\ spaces.file"#, format!("{}", actual))
    }
}
