use super::*;

pub struct CompileProblemPresenter<'a> {
    pub path_presenter: &'a RelativeFilePathPresenter<'a>,
}

impl<'a> Presenter<'a> for CompileProblemPresenter<'a> {
    type Data = CompileProblem;
    type Displayable = CompileProblemDisplay<'a>;

    fn render(&'a self, data: &'a Self::Data) -> Self::Displayable {
        CompileProblemDisplay {
            settings: self,
            data,
        }
    }
}
