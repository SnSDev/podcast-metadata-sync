use super::*;

pub struct UrlDisplay<'a> {
    pub data: &'a Url,
}

impl<'a> Display for UrlDisplay<'a> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> std::fmt::Result {
        write!(fmt, "{}", self.data)
    }
}
