use super::*;

#[derive(Default)]
pub struct PositionInFilePresenter {}

impl<'a> Presenter<'a> for PositionInFilePresenter {
    type Data = PositionInFile;
    type Displayable = PositionInFileDisplay<'a>;

    fn render(&'a self, data: &'a Self::Data) -> Self::Displayable {
        PositionInFileDisplay {
            _settings: self,
            data,
        }
    }
}
