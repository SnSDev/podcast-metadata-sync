pub trait Presenter<'a> {
    type Data;
    type Displayable: std::fmt::Display + 'a;
    fn render(&'a self, data: &'a Self::Data) -> Self::Displayable;
}
