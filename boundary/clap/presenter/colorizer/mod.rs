mod theme;
mod theme_match_vscode_dark_plus;
mod token_type;

pub use super::colorizer as prelude;
pub use theme::Theme;
pub use theme_match_vscode_dark_plus::ThemeMatchVscodeDarkPlus;
pub use token_type::TokenType;

use colored::{ColoredString, Colorize};
use colors_transform::{Color, Rgb};
