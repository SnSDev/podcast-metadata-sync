use super::*;

pub trait Theme {
    fn css_color_for(&self) -> &str;

    fn apply(&self, data: &str) -> ColoredString {
        let color = Rgb::from_hex_str(self.css_color_for()).unwrap();
        data.truecolor(
            color.get_red() as u8,
            color.get_green() as u8,
            color.get_blue() as u8,
        )
    }
}
