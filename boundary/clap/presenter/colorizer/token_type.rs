#[allow(dead_code)]
pub enum TokenType {
    FunctionDeclaration, // Function declarations
    TypeDeclaration,     // Types declaration and references
    TypeDeclarationTS, // Types declaration and references, TS grammar specific
    ControlFlow,       // Control flow / Special keywords
    VariableName,      // Variable and parameter name
    ConstantAndEnum,   // Constants and enums
    ObjectKeyTS,       // Object keys, TS grammar specific
    CSSProperty,       // CSS property value
    RgExGroup,         // Regular expression groups
    RegExConst,        // constant.character.character-class.regexp
    RegExKeyword,      // keyword.operator.or.regexp
    RegExQuantifier,   // keyword.operator.quantifier.regexp
    ConstChar,         // constant.character
    EscapeChar,        // constant.character.escape
    EntityName,        // entity.name.label
    NewOperator,       // newOperator
    StringLiteral,     // stringLiteral
    CustomLiteral,     // customLiteral
    NumberLiteral,     // numberLiteral
    Error,
    Warning,
    Info,
    Success,
    AutoFix,
    FileNotFound,
}
