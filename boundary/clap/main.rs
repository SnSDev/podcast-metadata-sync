//! Command Line Binary for [podcast_metadata_sync_lib]
//!
//! // TODO: Add documentation

mod build_clap_app;
mod config_data;
mod parse_matches;
// mod plug_in;
mod possible_match;
// mod presenter;
// mod sub_command;

use build_clap_app::build_clap_app;
use clap::{
    crate_authors, crate_description, crate_name, crate_version, App,
    AppSettings, ArgMatches,
};
use parse_matches::parse_matches;
use possible_match::ParsableMatch;
// use std::env;
// use sub_command::*;
use config_data::{ConfigData, ConfigLoaderError};

pub type MainReturn = std::io::Result<()>;
// pub type AddArgClosure<'a, 'b> = fn(App<'a, 'b>) -> App<'a, 'b>;
pub type AddSubCommandClosure<'a, 'b> = fn() -> App<'a, 'b>;
pub type ParseMatchClosure<'a> = fn(&ArgMatches<'a>) -> bool;
pub type RunUseCaseClosure<'a> = fn(&ArgMatches<'a>) -> MainReturn;

pub const SUB_COMMAND_ARRAY: [AddSubCommandClosure; 0] = [
//     list_files::build_sub_command,
//     parse_urls::build_sub_command,
//     crawl::build_sub_command,
];

pub const MATCH_ARRAY: [&ParsableMatch; 0] = [
//     &list_files::PARSABLE_MATCH,
//     &parse_urls::PARSABLE_MATCH,
//     &crawl::PARSABLE_MATCH,
];

fn main() -> MainReturn {
    let app = build_clap_app();
    let matches = app.get_matches();
    match parse_matches(&matches) {
        Some(use_case) => (use_case(&matches)),
        None => {
            match ConfigData::load_from_argument(matches.value_of("config")) {
                Ok(_) => {
                    println!("Config file loaded successfully");
                    Ok(())
                }
                Err(ConfigLoaderError::DeserializeError(e)) => {
                    Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        format!(
                            "1634568852 - Unable to parse config file {}",
                            e
                        ),
                    ))
                }
                Err(ConfigLoaderError::FileError(e)) => {
                    Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidInput,
                        format!("1634566479 - Config File Issue - {}", e),
                    ))
                }
                Err(ConfigLoaderError::NotSupplied) => {
                    Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidInput,
                        "1634568957 - Config File Not Specified",
                    ))
                }
            }
        }
    }
}
