use super::*;

pub fn parse_matches(
    matches: &ArgMatches<'static>,
) -> Option<RunUseCaseClosure<'static>> {
    MATCH_ARRAY.iter().find_map(|parsable_match| {
        if (parsable_match.check_for_match_closure)(matches) {
            Some(parsable_match.use_case_to_run)
        } else {
            None
        }
    })
}
