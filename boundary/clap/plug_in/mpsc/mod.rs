mod plug_in;

pub use plug_in::MpscInterThreadSender;

use markdown_database_lib::port::{InterThreadSender, InterThreadSenderError};
use std::sync::mpsc::{self, SendError};
