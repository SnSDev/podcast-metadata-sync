mod error;

pub use error::ConfigLoaderError;

use clap::crate_name;
use serde::Deserialize;
use std::path::{Path, PathBuf};

// #[cfg_attr(feature = "bin", derive(serde::Deserialize))]
#[derive(Debug, PartialEq, Deserialize)]
pub struct ConfigData {
    temp: bool,
}

pub fn default_config_path() -> PathBuf {
    dirs::preference_dir()
        .expect("1634565793 - Preferences Dir not loadable")
        .join(crate_name!())
        .join("podcast_metadata_sync.config.json")
}

impl ConfigData {
    pub fn load_from_path<P: AsRef<Path>>(
        file_path: P,
    ) -> Result<ConfigData, ConfigLoaderError> {
        Self::parse_from_string(
            &std::fs::read_to_string(file_path)
                .map_err(ConfigLoaderError::from)?,
        )
    }

    pub fn parse_from_string(
        file_contents: &str,
    ) -> Result<ConfigData, ConfigLoaderError> {
        serde_json::from_str(file_contents).map_err(ConfigLoaderError::from)
    }

    pub fn load_from_argument(
        config_arg: Option<&str>,
    ) -> Result<ConfigData, ConfigLoaderError> {
        match config_arg {
            Some("") => Err(ConfigLoaderError::NotSupplied),
            Some(c) if c.starts_with('{') => Self::parse_from_string(c),
            Some(c) => Self::load_from_path(c),
            None => Self::load_from_path(default_config_path()),
        }
    }
}
