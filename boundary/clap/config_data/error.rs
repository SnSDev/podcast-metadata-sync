pub enum ConfigLoaderError {
    DeserializeError(serde_json::Error),
    FileError(std::io::Error),
    NotSupplied,
}

impl From<serde_json::Error> for ConfigLoaderError {
    fn from(err: serde_json::Error) -> Self {
        Self::DeserializeError(err)
    }
}

impl From<std::io::Error> for ConfigLoaderError {
    fn from(err: std::io::Error) -> Self {
        Self::FileError(err)
    }
}
